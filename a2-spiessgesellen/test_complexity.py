#! /usr/bin/env python
from time import perf_counter_ns as timer
import matplotlib.pyplot as plt
import numpy as np
from memory_profiler import memory_usage
from main import parse, solve
from make_huge import make_huge

def test_complexity():
    nfruits, wish, skewers = parse(make_huge())
    mult = int(1e4)
    amount = len(skewers) // mult
    durations = np.zeros(amount)
    memory = np.zeros(amount)

    for i in range(1, amount):
        print(f"i: {i} / {amount}")
        start = timer()
        memory[i] = memory_usage((solve, (nfruits, wish, skewers[:i * mult])), max_iterations=1)[0]
        durations[i] = (timer() - start) / 1e6

    _, ax1 = plt.subplots()
    ax2 = ax1.twinx()

    ax1.set_xlabel(f"Amount of skewers / {mult}")
    ax1.set_xlim(1, amount)
    ax1.set_ylabel("Runtime in ms", color="C0")
    ax2.set_ylabel("Memory usage in mb", color='C1')
    ax1.plot(durations, label="Runtime")
    ax2.plot(memory, color="C1", label="Memory usage")
    plt.show()

if __name__ == "__main__":
    test_complexity()
