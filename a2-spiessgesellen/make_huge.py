#! /usr/bin/env python
import math
import itertools as it

# Create a huge example containing all combinations of fruits / bowls of k sizes ranging from `start` to `end` inclusive
# With the standard parameters, this means ~2.5M skewers (2516085)
def make_huge(start=5, end=8):
    problem = f"""26
    A C D F G H I J L M N O P Q S T U W X Y Z
    {sum(math.comb(26, k) for k in range(start, end + 1))}
    """
    fruits = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
    for k in range(start, end + 1):
        for comb in it.combinations(fruits, k):
            problem += ' '.join(str(ord(x) - 64) for x in comb) + '\n'
            problem += ' '.join(comb) + '\n'

    return problem

if __name__ == '__main__':
    from main import solve_string, get_answer
    print(get_answer(solve_string(make_huge())))
