# Aufgabe 2 - Spießgesellen

Donald's Problem lässt sich mittels Ausschlussvervahren lösen. Dazu sollte Donald zunächst alle existierenden Früchte einzeln betrachten. Für jede Frucht prüft er, ob sich die Frucht auf einem der beobachteten Spieße befindet. Falls dies der Fall ist, notiert er die Menge der Schüsseln, die die Person der der Spieß gehört aufgesucht hat. Daraufhin berechnet er die Schnittmenge dieser Mengen. Das sieht für Donalds Beispiel so aus:

```plain
Apfel:      {1 4 5} ∩ {1 2 4} = {1 4}
Banane:     {1 4 5} ∩ {3 5 6} = {5}
Brombeere:  {1 4 5} ∩ {1 2 4} = {1 4}
Pflaume:    {3 5 6} ∩ {2 6}   = {6}
Weintraube: {3 5 6}           = {3 5 6}
Erdbeere:   {1 2 4} ∩ {2 6}   = {2}
```

Nun weiß Donald bereits, dass sich in Schüssel 5 Bananen, in Schüssel 6 Pflaumen und in Schüssel 2 Erdbeeren befinden. Da die **zwei** Früchte Apfel und Brombeere beide die selbe Schüsselmenge besitzen und diese eben genau **zwei** Schüsseln enthält, kann Donald nicht eindeutig feststellen, welche der beiden Früchte in welche Schüssel gehört, ist aber nicht so schlimm, da er ja sowohl Apfel als auch Brombeere haben möchte. Er besucht also schonmal Schüssel 1 und 4.

Nun fehlt noch die Weintraube. Hierzu entfernt Donald alle eindeutig zugeordneten Schüssel - Frucht und Schüsselmenge - Fruchtmenge Paare aus seiner Liste. Außerdem streicht er die bereits zugeordneten Schüsseln aus den Schüsselmengen der verbleibenden Früchte. Das sieht dann so aus:

```plain
Weintraube: {3 5 6} \ {1 2 4 5 6} = {3}
```

Zum Glück ist Donalds Fall einfach, es bleibt nur eine Möglichkeit übrig! Weintrauben müssen sich in Schüssel 3 befinden.

Damit ist Donalds Rätsel gelöst. Um seinen Wunschspieß mit Apfel, Brombeere und Weintraube zu bekommen, muss er sich bei den Schüsseln 1, 3 und 4 anstellen.

## Lösungsidee

Das Lösungsprogramm funktioniert im Prinzip genauso wie oben beschrieben. Der einzige Unterschied ist, dass der letzte Schritt, das Streichen der eindeutigen Paare, so lange wiederholt wird, bis alle Früchte einer Schüssel bzw. einer Schüsselmenge die nicht weiter aufgeteilt werden kann zugeordnet sind.

## Umsetzung

Der Algorithmus berechnet eine allgemeine Frucht[menge] - Schüssel[menge] Zuordnung und gibt danach nur die für den Wunsch relevanten Informationen aus.

Zuerst liest die Methode `parse` einen String, der die Problemstellung enthält, ein und gibt die Anzahl der Früchte, die Menge der gewünschten Früchte und eine Liste der Spieße ausgibt. Ein Spieß (im Quellcode 'skewer') enthält die Menge der besuchten Schüsseln und die Menge seiner Früchte.

Die Methode `solve` erhält diese drei Werte als Parameter und berechnet aus ihnen die Lösung. Zunächst werden die Mengen `all_fruit` und `all_bowls` angelegt. Diese enthalten jeweils alle verfügbaren Obstsorten, die in der Datei genannt werden, und alle Schüsselnummern. `all_fruit` wird außerdem um 'Unbekannt &lt;i&gt;' erweitert, falls es mehr Obstsorten gibt als beobachtet wurden. Zum Beispiel gibt es insgesamt 4 Obstsorten, aber die Spieße (und der Wunsch) enthalten nur die Obstsorten A und B, dann würde `all_fruit` um `['Unbekannt 1', 'Unbekannt 2']` ergänzt werden, da zwei Obstorten fehlen. Die Schüsselnummern gehen von 1 bis Anzahl Sorten.

Danach wird eine Hashmap (Python `dict`) `possibilities` angelegt, die für jede Obstsorte die Menge der möglichen Schüsseln in denen sie sich befinden könnte enthält. Für alle Sorten wird dafür zuerst die Menge aller Schüsseln als Initialwert verwendet. Danach werden alle Spieße durchlaufen. Dabei wird für jede Obstorte auf dem aktuellen Spieß die zugehörige Schüsselmenge in `possibilities` auf die Schnittmenge mit der Menge der Schüsseln des Spießes eingeschränkt. Zum Beispiel ist `possibilities['A'] = {2,4,5}`, der aktuelle Spieß enthält die Frucht 'A' und die Schüsseln `{4,5,6}` wurden für den Spieß besucht, dann wäre nach diesem Schritt `possiblilties['A'] = {2,4,5} ∩ {4,5,6} = {4,5}`.

Nun kommt eine `while` Schleife, die wiederholt wird, solange in der Iteration ein Fortschritt erzielt wurde. Vorher wird eine weitere Hashmap `mapping` angelegt, welche für eine Menge von Obstsorten eine Menge von Schüsseln, in denen die Obstsorten enthalten sind beinhalten wird. Hier werden Mengen mit Mengen verknüpft, da es sein kann, dass klar ist, dass etwa zwei Obstsorten A und B in den Schüsseln 2 und 3 enthalten sind, aber nicht entschieden werden kann, welche der beiden Sorten nun in welche der beiden Schüsseln gehört.

Innerhalb der `while` Schleife wird `possibilities` durchlaufen. Zuerst werden alle Obstsorten gesammelt, die die gleichen möglichen Schüsseln haben wie die betrachtete Sorte. Ist die Anzahl dieser Sorten gleich der Anzahl an möglichen Schüsseln, so wurde ein Paar gefunden. Zur Veranschaulichung:

- Sorte = 'A', Möglichkeiten = {2} -> Paar {'A'}, {2}
- Sorte = 'A', Möglichkeiten = {3, 6} -> Weitere Sorte 'B' mit Möglichkeiten {3, 6} gefunden -> Paar {'A', 'B'}, {3, 6}

Wenn solch ein Paar gefunden wurde, kann nicht genauer bestimmt werden, welche der Sorten in welche der Schüsseln gehört, da alle möglichen Einschränkungen bereits vorgenommen wurden. Ein 1-1 Paar ist natürlich das Ziel, dies kann jedoch nicht immer erreicht werden.

Das gefundene Paar wird in `mapping` eingetragen, falls mindestens eine der Sorten gewünscht wurde, andernfalls brauchen wir das Paar nicht mehr. So oder so werden alle Sorten des Paares aus `possibilities` entfernt, um nicht weiter betrachtet zu werden. Außerdem wird für jede andere Sorte in `possibilities` die Menge der Schüsseln des Paares von der Menge der möglichen Schüsseln der Obstsorte abgezogen, da diese Schüsseln nun keine Möglichkeit mehr für andere Sorten sind.

- Sorte = 'A'; Möglichkeiten = {3, 4, 6}; Paar = {'B', 'C'}, {4, 6} -> neue Möglichkeiten = {3, 4, 6} \ {4, 6} = {3}

Sobald ein Paar gefunden wird, wird die aktuelle Iteration der `while` Schleife abgebrochen, da sich `possibilities` geändert hat. So werden nach und nach Paare extrahiert, bis alle Obstsorte aus `possibilities` entfernt wurden. Dann enthält `mapping` alle Paare, welche gewünschte Obstsorten enthalten.

Jetzt muss nur noch ein sinvolles Ergebnis erstellt werden. Dazu werden die Paare in zwei Kategorien aufgeteilt: eindeutig und uneindeutig. Eindeutig sind alle Paare, bei denen alle Obstsorten gewünscht sind. Uneindeutig sind Paare, die sowohl gewünschte als auch nicht gewünschte Sorten enthalten. Hierzu werden drei Listen angelegt: `known_fruit` und `known_bowls` für die Inhalte der eindeutigen Paare und `ambiguos` für die uneindeutigen Paare. Die eindeutigen Paare werden hier zusammengefasst, da es für Donald nicht wichtig ist, welche Frucht genau wo ist, sondern nur welche Schüsseln er alle besuchen muss. Um die Listen zu füllen werden alle Paare durchlaufen. Falls alle Obstsorten des Paares gewünscht wurden, werden die Sorten zu `known_fruit` und die Schüsseln zu `known_bowls` hinzugefügt. Falls nicht, wird das Paar zu `ambiguos` hinzugefügt.

Jetzt haben wir eine Liste von Schüsseln, die Donald auf jeden Fall besuchen muss um seine Wunschfrüchte zu bekommen und eine Liste von uneindeutigen Früchte - Schüsseln Paaren, bei denen sich Donald entscheiden muss ob er das Risiko eingehen möchte, sich unnötig anzustellen oder ob er diese Früchte lieber weglässt.

### Laufzeit & Komplexität

Mein Computer benötigt für die Berechnung aller Beispiele (außer das 'huge' Beispiel) mit samt I/O und Parsing circa 77 Millisekunden. Das 'huge' Beispiel (siehe 'Beispiele') braucht zur Berechnung ~3 Sekunden, das generieren und parsen des Beispiels benötigt zusätzlich 20 Sekunden. Somit ist I/O hier das Bottleneck. Da ich aber nicht davon ausgehe, das Donald Millionen von Spießkonfigurationen aufschreibt, sollte dies kein Problem sein.

Das Parsen hat eine Komplexität von O(n), da es nur einmal linear über die Eingabe geht. Der Lösungsalgorithmus hat ebenfalls eine Laufzeitkomplexität von O(n), wobei n die Menge der Spieße ist. Da die Anzahl der verschiedenen Früchte (k) auf 26 limitiert ist, ist die Menge der Spieße der ausschlaggebende Faktor. Aufgrund dieser Limitation werden hier auch die Komplexitäten der verwendeten Mengenoperationen außer Acht gelassen, da sie nur auf die limitierten Mengen von Früchten / Schalen angewendet werden.

Es wird lediglich zwei mal über die Liste der Spieße iteriert. Die restlichen Schleifen laufen aufgrund der Limitation of 26 Früchte nur bis zu 26 mal ab.

- Die `for` Schleifen welche über `possibilities` iterieren, können ebenfalls nur bis zu 26 Iterationen haben,
  da `possibilities` nur maximal 26 Früchte enthalten kann
- Die `while` Schleife entfernt pro Iteration mindestens eine Frucht, sodass sie maximal 26 Iteration hat
- Innerhalb der `while` Schleife wird `possibilities` ein geschachtelt durchlaufen, also O(k^2)
- Damit ist die Komplexität der `while` Schleife O(k^3), also 26^3 ~ 18 000 Schritte, wobei pro Durchlauf
  Elemente aus `possibilities` entfernt werden, also jeder Durchlauf weniger Schritte benötigt

Die Platzkomplexität des Algorithmus ist O(k^2), wobei k aber die Anzahl an verschiedenen Früchten / Schüsseln ist. Es werden neben Listen oder Mengen der Länge k (O(k)) Hashtables angelegt, welche alle maximal n Einträge mit der maximalen Länge k haben und somit O(k^2) Platz benötigen. Da k aber auf 26 limitiert ist, ist dieser Platzverbrauch vernachlässigbar, allein weil bei größeren Problemen der Speicherbedarf der Problembeschreibung (eigentlich nur die Liste der Spieße) den mit Abstand größten Teil des gesamten Speicherbedarfs ausmacht.

Die Laufzeit und den Speicherbedarf habe ich für verschiedene Mengen an Spießen getestet und zeichnen lassen:

![Komplexitätsgraph](./complexity.png)

Man sieht, dass die Laufzeit für n = 10000 bis 2.5 Millionen ungefähr linear ansteigt, wie vermutet. Der Speicherbedarf bleibt nahezu konstant, da ich beim Testen das Problem bereits ganz im Speicher habe, aber nur einen Teil lösen lasse. So sieht man, dass der Speicherbedarf des Lösungsalgorithmus, wie vermutet, nicht signifikant gegenüber dem der Problembeschreibung ist.

## Ausgabe & Beispiele

### Ausgabeformat

Falls das Programm eine eindeutige Lösung findet, gibt es die zu besuchenden Schüsseln als durch Komma getrennte Zahlen aus.

Falls die Lösung nicht eindeutig ist, sieht die Ausgabe folgendermaßen aus:

```plain
Nicht eindeutig:
Gewünschte Obstsorten:          {<Menge aller eindeutig bestimmten Obstsorten>}                           ] Diese zwei Zeilen werden nur ausgegeben,
befinden sich in den Schüsseln: {<Schüsseln, die die eindeutig bestimmten Obstsorten enthalten>}.         ] falls es eindeutig bestimmte Obstsorten gibt.
        Die Früchte                    {<Menge von Obstsorten, die min. eine gewünschte Frucht enthält>}  * Diese drei Zeilen werden
        befinden sich in den Schüsseln {<Menge der Schüsseln, die die genannten Obstsorten enthalten>},   * so oft wiederholt, wie
        gewünscht wurde aber nur       {<Schnittmenge der gewünschten Obstsorten und dieses Paares>}.     * es uneindeutige Paare gibt
```

### Beispielausgaben

Ausgabe zu allen vorgegebenen Beispielen:

```plain
Beispiel spiesse0 (aus der Aufgabe):
1, 3, 4

Beispiel spiesse1:
1, 2, 4, 5, 7

Beispiel spiesse2:
1, 5, 6, 7, 10, 11

Beispiel spiesse3:
Nicht eindeutig:
Gewünschte Obstsorten:          {Clementine, Erdbeere, Feige, Himbeere, Ingwer, Kiwi}
befinden sich in den Schüsseln: {1, 5, 7, 8, 10, 12}.
        Die Früchte                    {Grapefruit, Litschi}
        befinden sich in den Schüsseln {2, 11},
        gewünscht wurde aber nur       {Litschi}.


Beispiel spiesse4:
2, 6, 7, 8, 9, 12, 13, 14

Beispiel spiesse5:
1, 2, 3, 4, 5, 6, 9, 10, 12, 14, 16, 19, 20

Beispiel spiesse6:
4, 6, 7, 10, 11, 15, 18, 20

Beispiel spiesse7:
Nicht eindeutig:
Gewünschte Obstsorten:          {Clementine, Dattel, Mango, Sauerkirsche, Tamarinde, Vogelbeere, Yuzu, Zitrone}
befinden sich in den Schüsseln: {5, 6, 8, 14, 16, 17, 23, 24}.
        Die Früchte                    {Apfel, Grapefruit, Litschi, Xenia}
        befinden sich in den Schüsseln {26, 10, 3, 20},
        gewünscht wurde aber nur       {Apfel, Grapefruit, Xenia}.
        Die Früchte                    {Banane, Ugli}
        befinden sich in den Schüsseln {25, 18},
        gewünscht wurde aber nur       {Ugli}.
```

### Eigene Beispiele

Ich habe fünf eigene Beispiele erstellt, die Edgecases testen. Die Eingabedateien lassen sich unter `a2-spiessgesellen/input` finden.

#### Beispiel 'empty'

Hier wird der Fall getestet, das keine Spieße beobachtet wurden. Da es aber 5 Obstsorten gibt und die 5 Sorten `A, B, C, D, E` gewünscht wurden, sollte es eine eindeutige Lösung geben:

```plain
Beispiel empty:
1, 2, 3, 4, 5
```

#### Beispiel 'empty_skewer'

Hier werden 3 von 5 Obstsorten gewünscht und ein Spieß beobachtet, nur hat dieser keinen Inhalt. Es sollte eine uneindeutige Lösung geben, die zwei unbekannte Früchte nennt:

```plain
Beispiel empty_skewer:
Nicht eindeutig:
        Die Früchte                    {B, C, E, Unbekannt 1, Unbekannt 2}
        befinden sich in den Schüsseln {1, 2, 3, 4, 5},
        gewünscht wurde aber nur       {B, C, E}.
```

#### Beispiel 'no_wish'

Hier wurden keinerlei Obstsorten gewünscht. Diesen Fall habe ich gesondert behandelt:

```plain
Beispiel no_wish:
Keine Früchte wurden gewünscht
```

#### Beispiel 'unknown_fruit'

Hier werden 3 von 6 Obstsorten gewünscht, und 2 Weitere sind auf einem Spieß, sodass eine unbekannte Frucht bleibt. Es sollte ein uneindeutiges Ergebnis entstehen:

```plain
Beispiel unknown_fruit:
Nicht eindeutig:
        Die Früchte                    {A, B, C, Unbekannt 1}
        befinden sich in den Schüsseln {3, 4, 5, 6},
        gewünscht wurde aber nur       {A, B, C}.
```

#### Beispiel 'huge'

Dieses Beispiel wird generiert (siehe `a2-spiessgesellen/make_huge.py`). Es beinhaltet 26 Obstsorten und ca. 2,5 Millionen Spieße. Es ergibt sich eine eindeutige Lösung:

```plain
Beispiel 'huge':
1, 3, 4, 6, 7, 8, 9, 10, 12, 13, 14, 15, 16, 17, 19, 20, 21, 23, 24, 25, 26
```

## Implementierung

### Übersicht

Sprache: Python 3.9
Testsystem: Linux 5.10

- Aller für den Lösungsweg wichtiger Code befindet sich in `a2-spiessgesellen/main.py`
- Wird diese Datei ausgeführt, versucht sie eine Problemstellung aus STDIN zu lesen
- Die restlichen Dateien sind für Testzwecke
- `run.py` führt alle Beispiele aus

- Im Code sind Kommentare auf Englisch vorhanden

Code des Lösungsalgorithmus:

```python
# Spieße (skewer) werden als tuple[set[int], set[str]] modelliert (Menge der Schüsseln, Menge der Obstsorten)

def solve(nfruits: int, wish: set[int], skewers: list[tuple[set[int], set[str]]]):
    """Solves a problem described by number of fruits, wished fruits and known skewers.
    Returns a tuple of sorted lists (wished fruits, known fruits, known bowls, ambiguos mappings)
    """
    # Collections of all available fruits and bowls
    all_fruit = set.union(wish, *[f for _, f in skewers])
    all_bowls = set(range(1, nfruits + 1))

    # Add dummy fruits for excess bowls
    all_fruit = all_fruit.union(f"Unbekannt {i + 1}" for i in range(len(all_bowls) - len(all_fruit)))

    # Initialize the possibilities for all fruits to be any bowl
    possibilities = dict()
    for f in all_fruit:
        possibilities[f] = all_bowls

    # Create a mapping {fruit -> possible bowls} by computing the intersection
    # of all sets of bowls the fruit could be in as specified by the skewers
    for bowls, fruits in skewers:
        for fruit in fruits:
            possibilities[fruit] = possibilities[fruit] & bowls

    # Repeatedly extract fruit[s]-number[s] mappings that are definite until everything is extracted from possibilities
    mapping = dict()
    while len(possibilities) > 0:
        # Check each fruit that is still not definitely mapped
        for fruit, options in possibilities.items():
            # Get all fruits that have the exact same possible bowls as the current fruit
            fruits = [f for f, o in possibilities.items() if o == options]

            # If the amount of possibilities is the same as the amount of fruits having these possibilities,
            # this means that we cannot further specify the mapping of these fruits to these bowls.
            if len(options) == len(fruits):
                # Save the fruits - bowls association if any of the contained fruits was wished,
                # otherwise we do not need it anymore
                if any(f in wish for f in fruits):
                    mapping[tuple(sorted(fruits))] = tuple(options)

                # Delete the extracted fruits from our schedule
                for f in fruits:
                    del possibilities[f]

                # Remove the extracted bowls from all other possibility sets
                # as they are no longer a possibility for any other fruit besides
                # our extracted ones
                for other, _ in possibilities.items():
                    possibilities[other] = possibilities[other] - options

                # Skip to the next iteration because we just extracted a pair
                break

    # Create a usable result
    known_fruit = [] # Will the set wished for fruits that we can assign a set of bowls
    known_bowls = [] # Will contain the set of bowls which contain the known fruits
    ambiguos = [] # Will contain (fruits, bowls) tuples of n to n fruits - bowls mappings that contain unwanted fruits

    for fruits, bowls in mapping.items():
        # Add all fruit and bowls to the respective list if all fruits in the mapping were wished for
        if all(f in wish for f in fruits):
            known_fruit.extend(fruits)
            known_bowls.extend(bowls)
        # Else our mapping sadly contains some fruits which were wished for,
        # but also some that were not. So put them into our ambiguos list
        else:
            ambiguos.append((fruits, bowls))

    # Return a tuple of sorted lists (wished fruits, known fruit, known bowls, ambiguos mappings)
    return (sorted(list(wish)), sorted(known_fruit), sorted(known_bowls), ambiguos)
```

Code für die Erstellung der Antwort:

```python
def get_answer(solution):
    """Turn a solution tuple obtained from `solve` or `solve_string` into a human readable answer string."""
    wish, known_fruit, known_bowls, ambiguos = solution

    # Edgease 1: No wished fruits
    if len(wish) == 0:
        return "Keine Früchte wurden gewünscht"

    # Edgease 2: No ambiguities
    if len(ambiguos) == 0:
        return ', '.join(map(str, known_bowls))

    # Standard case: Some or no unambiguos mappings, and some ambiguities
    s = "Nicht eindeutig:\n"
    if len(known_fruit) > 0:
        s += f"""\
Gewünschte Obstsorten:          {{{', '.join(known_fruit)}}}
befinden sich in den Schüsseln: {{{', '.join(map(str, known_bowls))}}}.\n"""

    # Add all ambiguities
    for fruits, bowls in ambiguos:
        s += f"""\tDie Früchte                    {{{', '.join(fruits)}}}
\tbefinden sich in den Schüsseln {{{', '.join(map(str, bowls))}}},
\tgewünscht wurde aber nur       {{{', '.join(f for f in fruits if f in wish)}}}.\n"""

    return s
```
