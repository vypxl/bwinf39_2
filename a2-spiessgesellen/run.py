#! /usr/bin/env python
from time import perf_counter_ns as timer
from main import get_answer, parse, solve, solve_string
from make_huge import make_huge

def main():
    for i in range(8):
        print(f"Beispiel spiesse{i}:")
        inp = open(f'input/spiesse{i}.txt').read()
        run_testcase(inp)
        print()
    for name in ['empty', 'empty_skewer', 'no_wish', 'unknown_fruit']:
        print(f"\nBeispiel {name}:")
        inp = open(f'input/{name}.txt').read()
        run_testcase(inp)
    print("Beispiel 'huge':")
    run_testcase(make_huge())


def run_testcase(test_string, timing=False):
    if not timing:
        solution = solve_string(test_string)
    else:
        nfruits, wish, skewers = parse(test_string)
        start = timer()
        solution = solve(nfruits, wish, skewers)
        duration = timer() - start

    answer = get_answer(solution)
    print(answer)
    if timing:
        print(f"Duration: {round(duration / 1e6, 2)}ms")


if __name__ == "__main__":
    main()
