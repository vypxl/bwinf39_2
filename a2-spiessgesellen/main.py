#! /usr/bin/env python
def parse(s):
    """Parse a problem statement given as a string.
    Returns a tuple of (Number of fruits, wished fruits, known skewers)
    """
    readSkewer = lambda bowls, fruits: (set(int(x) for x in bowls.split()), set(fruits.split()))

    lines = s.splitlines()
    nfruits = int(lines[0])
    wish = set(lines[1].split())
    nskewers = int(lines[2])
    skewers = [readSkewer(lines[3 + 2 * i], lines[4 + 2 * i]) for i in range(nskewers)]

    return (nfruits, wish, skewers)

def solve(nfruits, wish, skewers):
    """Solves a problem described by number of fruits, wished fruits and known skewers.
    Returns a tuple of sorted lists (wished fruits, known fruits, known bowls, ambiguos mappings)
    """
    # Collections of all available fruits and bowls
    all_fruit = set.union(wish, *[f for _, f in skewers])
    all_bowls = set(range(1, nfruits + 1))

    # Add dummy fruits for excess bowls
    all_fruit = all_fruit.union(f"Unbekannt {i + 1}" for i in range(len(all_bowls) - len(all_fruit)))

    # Initialize the possibilities for all fruits to be any bowl
    possibilities = dict()
    for f in all_fruit:
        possibilities[f] = all_bowls

    # Create a mapping {fruit -> possible bowls} by computing the intersection
    # of all sets of bowls the fruit could be in as specified by the skewers
    for bowls, fruits in skewers:
        for fruit in fruits:
            possibilities[fruit] = possibilities[fruit] & bowls

    # Repeatedly extract fruit[s]-number[s] mappings that are definite until everything is extracted from possibilities
    mapping = dict()
    while len(possibilities) > 0:
        # Check each fruit that is still not definitely mapped
        for fruit, options in possibilities.items():
            # Get all fruits that have the exact same possible bowls as the current fruit
            fruits = [f for f, o in possibilities.items() if o == options]

            # If the amount of possibilities is the same as the amount of fruits having these possibilities,
            # this means that we cannot further specify the mapping of these fruits to these bowls.
            if len(options) == len(fruits):
                # Save the fruits - bowls association if any of the contained fruits was wished,
                # otherwise we do not need it anymore
                if any(f in wish for f in fruits):
                    mapping[tuple(sorted(fruits))] = tuple(options)

                # Delete the extracted fruits from our schedule
                for f in fruits:
                    del possibilities[f]

                # Remove the extracted bowls from all other possibility sets
                # as they are no longer a possibility for any other fruit besides
                # our extracted ones
                for other, _ in possibilities.items():
                    possibilities[other] = possibilities[other] - options

                # Skip to the next iteration because we just extracted a pair
                break

    # Create a usable result
    known_fruit = [] # Will the set wished for fruits that we can assign a set of bowls
    known_bowls = [] # Will contain the set of bowls which contain the known fruits
    ambiguos = [] # Will contain (fruits, bowls) tuples of n to n fruits - bowls mappings that contain unwanted fruits

    for fruits, bowls in mapping.items():
        # Add all fruit and bowls to the respective list if all fruits in the mapping were wished for
        if all(f in wish for f in fruits):
            known_fruit.extend(fruits)
            known_bowls.extend(bowls)
        # Else our mapping sadly contains some fruits which were wished for,
        # but also some that were not. So put them into our ambiguos list
        else:
            ambiguos.append((fruits, bowls))

    # Return a tuple of sorted lists (wished fruits, known fruit, known bowls, ambiguos mappings)
    return (sorted(list(wish)), sorted(known_fruit), sorted(known_bowls), ambiguos)

def solve_string(input_string):
    """ Solve the problem statement given as a string.
    Returns a tuple of sorted lists (wished fruits, known fruit, known bowls, ambiguos mappings)
    """
    nfruits, wish, skewers = parse(input_string)
    return solve(nfruits, wish, skewers)

def get_answer(solution):
    """Turn a solution tuple obtained from `solve` or `solve_string` into a human readable answer string."""
    wish, known_fruit, known_bowls, ambiguos = solution

    # Edgease 1: No wished fruits
    if len(wish) == 0:
        return "Keine Früchte wurden gewünscht"

    # Edgease 2: No ambiguities
    if len(ambiguos) == 0:
        return ', '.join(map(str, known_bowls))

    # Standard case: Some or no unambiguos mappings, and some ambiguities
    s = "Nicht eindeutig:\n"
    if len(known_fruit) > 0:
        s += f"""\
Gewünschte Obstsorten:          {{{', '.join(known_fruit)}}}
befinden sich in den Schüsseln: {{{', '.join(map(str, known_bowls))}}}.\n"""

    # Add all ambiguities
    for fruits, bowls in ambiguos:
        s += f"""\tDie Früchte                    {{{', '.join(fruits)}}}
\tbefinden sich in den Schüsseln {{{', '.join(map(str, bowls))}}},
\tgewünscht wurde aber nur       {{{', '.join(f for f in fruits if f in wish)}}}.\n"""

    return s


# Try to read and solve a problem from stdin if called as main file
if __name__ == "__main__":
    import sys
    print(get_answer(solve_string(sys.stdin.read())))
