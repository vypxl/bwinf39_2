### A Pluto.jl notebook ###
# v0.14.2

using Markdown
using InteractiveUtils

# ╔═╡ 40b4099c-690f-403f-a638-0d0d388d5da9
begin
	using PlutoUI: LocalResource as img
	img("./complexity1.svg")
end

# ╔═╡ 75e64786-8676-11eb-3597-f79f94b517ed
using Combinatorics: combinations;

# ╔═╡ 528c7bac-8676-11eb-1e17-fff90e96c3c6
md"""
# Aufgabe 3 - Eisbudendilemma
"""

# ╔═╡ 40371f7f-7415-46a6-b8ce-2e8753341de8
md"""
Wer auch immer sich dieses Gesetz ausgedacht hat, hat den Seebewohner in ein gehöriges Problem verschafft.
Für einen See des Umfangs ``c`` (in Schritten; englisch 'circumference') und die Anzahl der Eisbuden ``k`` gibt es ``c \choose k`` mögliche Eisbudenkonfigurationen, wenn Eisbuden nur an ganzzahligen Schrittpositionen platziert werden können, wovon in dieser Lösung ausgegangen wird, da sich die Häuser ebenfalls nur an ganzzahligen Positionen befinden.

Nun gilt es, Konfigurationen zu finden die 'stabil' sind, die also in einer Abstimmung gegen keine Andere verlieren würden.

Dabei ist zu beachten, dass sich Konfigurationen nicht per Turnier aussortieren lassen. Eine Konfiguration A, die gegen die Konfiguration B in einer Abstimmung gewinnt, kann gegen eine Konfiguration C verlieren, auch wenn B gegen C gewinnt. Ähnlich wie bei Schere-Stein-Papier gibt es in diesem Fall keinen Gesamtsieger. Diese Eigenschaft des Problems lässt sich belegen, indem man eine Problemstellung findet, für die es keine stabile Konfiguration gibt, denn dann muss es mindestens einen solchen Zyklus geben, da ansonsten mindestens eine Konfiguration gegen alle anderen gewonnen hätte. Die vorgegebenen Beispiele 2,4 und 6 haben alle keine stabile Konfiguration, was durch eine vollständige Suche festgestellt wurde. Somit ist diese Eigenschaft bewiesen.

Dies bedeutet für den Lösungsalgorithmus, dass er keinerlei Annahmen über die Stabilität einer Konfiguration anhand vorheriger Abstimmungen machen kann.
"""

# ╔═╡ e53464a8-8a47-4019-af31-c9a6569f108d
md"""
## Lösungsidee

Stabile Konfigurationen lassen sich finden, indem man sie gegen jede andere mögliche Konfiguration in einer Abstimmung antreten lässt. Ist sie in keiner Abstimmung unterlegen, so handelt es sich um eine stabile Konfiguration. Der Algorithmus wird durch den folgenden Pseudocode beschrieben:
```
für jede Konfiguration k tue
	für jede Konfiguration l tue
		falls l gegen k gewinnt (k ist instabil)
			gehe zur nächsten Konfiguration k
	gib k zurück, da keine andere Konfiguration k besiegt hat
```

Es gibt ``c \choose k`` Möglichkeiten, eine Konfiguration von k Buden an einem See des Umfangs c zu bilden. ``c \choose k`` lässt sich in einen ganzrationalen Term umschreiben. Bei k=3 gilt: ``{c \choose 3} = \frac{{c}^{3}}{{6}}-\frac{{c}^{2}}{{2}}+\frac{c}{{3}}``. Da außerdem gilt ``{c \choose k} = {c \choose c-k}`` wächst ``c \choose k`` näherungsweise mit ``O(c^{min(k,c-k)})``, sodass dieses Vorhaben eine Komplexität von insgesamt ``O((c^{min(k,c-k)})^2)``, also ``O(c^{2*min(k,c-k)})`` hat.

Für den in der Aufgabenstellung genannten Fall ``k = 3`` (angenommen ``c>=6``) wäre die Komplexität eines simplen Complete-Search Algorithmus also ``O(c^6)``.

Trotz intensiver analytischer Bemühungen ließ sich leider kein einfach zu berechnendes Kriterium für die Stabilität einer Konfiguration herausfinden. Also muss ein guter Weg gefunden werden, den Suchraum für potenzielle stabile Konfigurationen geeignet einzuschränken und schnell Instabilität festzustellen.


Da es nicht leicht ist, die Stabilität einer Konfiguration festzustellen, wurde sich darauf konzentriert eine stabile Konfiguration zu finden, nicht alle Möglichen. Auch sollte dies dem Anwendungsfall genügen, schließlich brauchen die Dorfbewohner nur einen Satz an Eisbudenpositionen, welcher für immer unumstößlich bestehen wird, damit das ewige Umsetzen ein Ende hat.

Das bedeutet auch, dass diese Lösung besser Beispiele mit einer stabilen Konfiguration  lösen kann als Beispiele ohne, da bei der ersten stabilen Konfiguration vorzeitig abgebrochen werden kann.
"""

# ╔═╡ 9cdf9527-6daf-44fe-b13d-4978f9be1301
md"""
## Simulation der Abstimmung

Um stabile Konfigurationen zu finden, müssen diese gegen Andere in einer Abstimmung antreten. Dies funktioniert folgendermaßen:

- Für beide Konfigurationen werden die Abstände jedes Hauses zur nächsten Eisbude berechnet
- Danach wird gezählt, bei wie vielen Häusern sich dieser Abstand verbessert/verschlechtert hat
- Ist bei einer der Konfigurationen festzustellen, dass sich die Situation für mehr als die Hälfte der Häuser gegenüber der anderen Konfiguration verbessert hat (der Abstand ist kleiner geworden), so gewinnt diese die Abstimmung
- Tritt für keine der Konfigurationen eine absolute Mehrheit ein, so ist die Abstimmung unentschieden, das heißt, dass keine der Konfigurationen gegen die andere eingetauscht werden würde

Hierbei ist zu beachten, dass ein Kreis vorliegt. Das bedeutet, dass der Abstand eines Hauses zu einer Eisbude nach links oder rechts zu berechnen ist, je nach dem, in welcher Richtung die Eisbude näher ist. Die folgende Formel beachtet dieses Detail:

``Abstand_c(x, y) = |((y - x + \frac{c}{2})\ \%\ c) - \frac{c}{2}|``

So ist beispielsweise der Abstand der Punkte 18 und 3 auf einem Kreis des Umfangs 20:

``Abstand_{20}(18, 3) = |((3 - 18 + \frac{20}{2})\ \%\ 20) - \frac{20}{2} = |(-5\ \%\ 20) - 10| = |15 - 10| = 5``
"""

# ╔═╡ b6736afd-5524-48f5-a406-498adaf35278


# ╔═╡ abba2b0e-a611-44fe-8175-dc6e1af2c05c
md"""
## 1. Optimierung - Speicherung der Abstände

Um Zeit bei der häufig stattfindenden Abstandsberechnung zu sparen, werden für jede mögliche **einzelne** Eisbudenposition die Abstände zu jedem Haus im Voraus berechnet und zwischengespeichert. Das führt dazu, dass bei der Simulation einer Abstimmung nur noch für jedes Haus das Minimum der k vorher berechneten Abstände zu den Eisbuden gefunden werden muss (für beide Konfigurationen), was Zeit spart. In der Praxis läuft das Programm hiermit sogar 3-10fach schneller, je nach Beispiel. Dabei brauch diese Optimierung nur ``O(n*c)`` Speicher, was selbst bei größeren Beispielen nicht ins Gewicht fällt.

Man könnte nun auf den Gedanken kommen, ebenfalls für ganze Konfigurationen die minimalen Abstände zu speichern. Für das 7. Beispiel gibt es für k=3 ``{741 \choose 3} = 67.537.210`` Konfigurationen. Um die Abstände naiv zu speichern, könnte man ein Array mit 4 Dimensionen verwenden. Diesen würde man in C so notieren: `short[741][741][741][40]`, 3 Dimensionen der Größe ``c`` für die drei Positionen der Konfiguration und eine Dimension der Größe ``n`` für die Abstände der Häuser zur nächsten Eisbude. Solch ein Array hätte, wenn man einen Ganzzahl-Datentyp der Größe 2 Bytes verwendet, da dieser mehr als ausreicht um alle in Frage kommenden Zahlen abzubilden, die Größe von 30,31 GigaByte, was für die meisten heutigen Computer nicht im RAM speicherbar ist.

Würde man für jede der Konfigurationen die Abstände der 40 Häuser in einer Hashmap speichern, bräuchte man ``{741 \choose 3} * (40 * 2 + 8) = 5.54gb`` (bei einem 64-Bit Hash, ausgenommen den Overhead des Datentyps), was schon eher machbar scheint. In der Praxis stellt sich dieser Ansatz als nutzlos heraus, da es länger dauert, die Abstände aus der Tabelle zu lesen, als sie einfach neu zu berechnen (unter Verwendung der gespeicherten Einzelabstände). Zumindest trifft dies für den Julia-Datentyp `Dict` zu, welchen ich zum Testen verwendet habe.

Da der simple Array für größere Beispiele zu Speicherintensiv ist, und eine Lookup-Table zu langsam, bietet sich ein Array von Zeigern an. Jeder Zeiger würde auf die Abstände zu einer Konfiguration zeigen. Da nur die einzigartigen Konfigurationen gespeichert werden müssen, bräuchte dieser Array auf einem 64-Bit System (Zeigergröße 8 Bytes) für das 7. Beispiel ``741^3 * 8 + {741 \choose 3} * 40 * 2 = 8,06gb``. Dies ist zwar deutlich mehr als bei der Hashmap, sollte aber für Computer mit mindestens 16gb RAM machbar sein, da die Abstände selbst nicht alle gemeinsam als Block gespeichert werden müssen. Trotzdem bedarf es hierfür einer Überprüfung, ob der Computer des Bürgermeisters denn genug RAM für das Vorhaben besitzt. Falls diese zu dem Schluss kommt, das der RAM unzureichend ist, muss ohne diese Optimierung fortgefahren werden. 

Bei wachsenden k-Werten nimmt der Speicherbedarf exponentiell zu. Deswegen wird dabei schnell das Speicherlimit erreicht.

In der vorliegenden Implementierung führt diese Optimierung nur bei nicht lösbaren Beispielen zu einer signifikanten Zeitersparnis. Das liegt daran, dass Beispiele ohne Lösung nicht bei der ersten gefundenen Lösung abbrechen können, sondern alle durchprobieren. So werden viel öfter die Abstände berechnet und die kleine Ersparnis durch die Speicherung fällt mehr ins Gewicht. Für Beispiel 6 etwa braucht das Programm mit dieser Optimierung nur ca. ``\frac{2}{3}`` der Zeit gegenüber der nicht optimierten Variante.

Bei größeren, lösbaren Problemen führt diese Optimierung in Einzelfällen sogar zu längeren Laufzeiten. Dies liegt allerdings daran, dass Julia lange braucht um so viele `Ref` Objekte zu bilden, welche hier als Zeiger verwendet werden. Da dieses Problem aber eines der verwendeten Programmiersprache ist, wurde es nicht weiter betrachtet. Die Average-Case Laufzeit wird durch diese Optimierung trotz der Einzelfälle deutlich heruntergesetzt.
"""

# ╔═╡ 4460e7a9-0f46-401d-8171-c03f4762afa6
md"""
## 2. Optimierung - Sortierung der Konfigurationen nach Gesamtabstand

Als gute Heuristik für vielversprechende Konfigurationen hat sich der 'Gesamtabstand' einer Konfiguration, also die Summe der Abstände aller Häuser zu ihrer nächsten Eisbude. Hier lässt sich ein Optimierungsproblem ableiten (die Minimierung des Gesamtabstandes), welches mit dem 'Multiway Number Partitioning' Problem verwandt ist.
Da der Gesamtabstand aber nicht direkt mit der Stabilität korreliert ist, kann das Originalproblem leider nicht auf dieses heruntergebrochen werden.

Trotzdem lässt sich, wenn man die Konfigurationen nach ihrem Gesamtabstand sortiert, deutlich schneller eine Lösung finden, da Konfigurationen mit niedrigem Gesamtabstand dazu tendieren, stabil zu sein, oder zumindest gegen viele andere Konfigurationen in einer Abstimmung zu gewinnen.

Diese Optimierung führt eine Startphase des Programms ein, welche alle Konfigurationen generieren und speichern muss, um diese dann zu sortieren. Dies kann, grade bei größeren Beispielen, zwar Zeit fressen, führt aber zu einer wesentlichen Zeitersparnis im restlichen Programm.

Der Speicherbedarf hält sich hierbei in Grenzen: Da ``c \choose k`` mit ``O(c^{min(k,c-k)})`` wächst, gilt dies auch für die Speicherkomplexität dieser Optimierung. Jede Konfiguration benötigt dabei ``k`` gespeicherte Zahlen. Für das 7. Beispiel kommt man konkret auf ``{741 \choose 3} * 3 * 2 = 0.38gb`` (bei 2-Byte Integern). Es kann durchaus aufteten, dass der verfügbare Arbeitsspeicher nicht ausreicht, um die Kombinationen zu speichern. Dies passiert aber nur bei Beispielen, die so groß sind, dass sie so oder so nicht in annehmbarer Zeit gelöst werden können. Deswegen wurde dieser Fall nicht beachtet.

Die einmal sortierte Konfigurationsreihenfolge bleibt gespeichert und wird sowohl bei der Suche nach stabilen Konfigurationen, als auch bei der Stabilitätsprüfung eingesetzt. So kann, wenn nur eine stabile Konfiguration gesucht ist, solch eine schnell gefunden werden, da mindestens eine davon meistens (zumindest in allen durchgeführten Tests) am Anfang der Reihenfolge zu finden ist. Genauso ist es bei der Stabilitätsprüfung. Fast alle instabilen Konfigurationen scheitern an den ersten par Konfigurationen der Reihenfolge. So können diese schnell aussortiert werden.

Diese Optimierung hat den zusätzlichen Nebeneffekt, dass die erste stabile Konfiguration die gefunden wird gleichzeitig die mit dem geringsten Gesamtabstand ist. Das heißt im Kontext, dass die Dorfbewohner mit dieser Konfiguration im Durchschnitt am wenigsten weit zu den Eisbuden laufen müssen.

Der Suchraum bei der Stabilitätsprüfung (Konfigurationen gegen die angetreten werden muss) kann durch dieses Kriterium nicht eingeschränkt werden, da es in Tests teilweise, wenn auch selten, dazu kam, das sogar eine Konfiguration im hinteren Viertel der Verursacher einer Instabilität war. So lässt sich nicht auf eine definitive Abgrenzung des Suchraums der Konfigurationen anhand des Gesamtabstandes schließen.

Der Suchraum der stabilen Konfigurationen lässt sich so ebenfall nicht einschränken. In Tests mit größeren Beispielen ergab sich zwar keine einzige stabile Konfiguration die nicht in den top 10% der sortierten Konfigurationen war, bei kleineren jedoch schon. Somit lässt sich auch hier nicht auf eine definitive Abgrenzung des Suchraums schließen.
"""

# ╔═╡ 91b04923-d4c9-4a85-a2eb-cddcf8b783a0
md"""
## Speicherung von Abstimmungsergebnissen

Bei einen unoptimierten jeder-gegen-jeden Lösung käme es oft dazu, dass Abstimmungen mehrfach durchgeführt werden. Wenn beispielsweise bei der ersten betrachteten Konfiguration diese gegen die ersten drei Gegnerkonfigurationen gewonnen hätte und durch die vierte instabil wurde, müsste man eigentlich nicht mehr die drei Gegnerkonfigurationen in Betracht ziehen, da sie bereits einmal besiegt wurden. Sie können also gar nicht mehr stabil sein.

Nun könnte man etwa in einem Bit-Array speichern, welche Konfigurationen bereits mindestens einmal besiegt wurden, um diese dann bei der Suche nach stabilen Konfigurationen zu überspringen.

In der Praxis stellt sich jedoch heraus, dass unter Verwendung der anderen beiden genannten Optimierungen keine signifikante Zeitersparnis eintritt. Dies liegt vermutlich daran, dass die Konfigurationen, die auf diese Weise übersprungen werden könnten, sowieso sehr schnell durch eine der Konfigurationen mit niedrigem Gesamtabstand aussortiert werden.

Da diese Optimierung zu keiner messbaren Zeitersparnis führt, wurde sie wieder entfernt. Zudem ist der Quelltext so lesbarer.
"""

# ╔═╡ 7ef9de81-b66f-417d-b7cc-8ceebb346520
md"""
## Erweiterung: Andere k-Werte

Die vorliegende Lösung ist in der Lage, stabile Konfigurationen für beliebige Anzahlen an Eisbuden zu finden. Der Algorithmus bleibt dabei gleich, nur die Generierung der Konfigurationen wird abgeändert.

Diese Erweiterung des Problems kann dafür genutzt werden, stabile Konfigurationen von Eisbuden zu finden, auch wenn es für k=3 keine gibt. So können die Seebewohner, falls sie es in Kauf nehmen, eine zusätzliche Eisbude zu errichten (oder eine abzureißen) trotzdem in Frieden ohne ständige Eisbudenverschiebungen Leben.
"""

# ╔═╡ b4606c54-ee79-4bd0-9ca8-c4107cdb67d4
md"""
## Laufzeitverhalten

Wie oben bereits beschrieben hat der vorliegende Lösungsalgorithmus eine Worst-Case Komplexität von ``O(c^{2*min(k, c-k)})``. Das bedeutet, dass bei konstantem k die Laufzeit polynomial im Verhältnis zum Umfang des Sees wächst. Bei dem in der Aufgabenstellung genannten Fall k=3 gilt also theoretisch ``O(c^6)``.

Mit den Optimierungen sieht es deutlich besser aus:
In den folgenden Graphen sieht man für k=3 und c=[10; 75] die durchschnittliche Laufzeit von 100 zufällig generierten Beispielen (zufällige Anzahl und Position der Häuser) in Sekunden (blaue Punkte), ohne und mit Minimalabstandsspeicherung. Die orangene Kurve wurde an diese Punkte angepasst, ausgehend von einer Funktion der Form ``f(c) = a * c^{b*k}``
"""

# ╔═╡ a856419b-149a-4eea-ad00-a4cbfd561afe
md"Ohne Minimalabstandsspeicherung:"

# ╔═╡ bdeae36c-0daf-44e0-96f8-7226f13dde2b
md"Mit Minimalabstandsspeicherung:"

# ╔═╡ 7d4d2336-b5fe-4458-8cff-164ad71a28c8
img("./complexity2.svg")

# ╔═╡ cbdb4b0b-7ef1-4b8e-bac2-5d263887810e
md"""
Die angepassten Parameter ``a`` und ``b`` sind (ohne -speicherung) ``a = 2.7*10^{-10}`` und ``b = 1.5`` respektive (mit -speicherung) ``a = 1.2*10^{-9}`` und ``b = 1.4``.
Somit kann die durchschnittliche Laufzeit des Programms zum Fall k=3 näherungsweise mit der folgenden Funktion berechnet werden: ``f(c) = (2.7 * 10^{-10})c^{4.5} Sekunden`` / ``f(c) = (1.2 * 10^{-9})c^{4.2} Sekunden``. Zumindest passen diese Funktionen für `c` im Intervall `[10; 75]`. Man kann anhand des kleineren Exponenten bei der Funktion mit -speicherung erkennen, dass durch die Speicherung eine durchaus signifikante Zeitersparnis erreicht wird.

Für größere ``c`` kann es durchaus zu sehr großen Abweichungen kommen. Das siebte vorgegebene Beispiel kann (auf meinem Computer) in 35 Sekunden (ohne Minimalabstandsspeicherung) beziehungsweise 80 Sekunden (mit -speicherung) berechnet werden (warum langsamer? Siehe 1. Optimierung letzter Absatz). Die Funktion sagt jedoch eine Zeit von 2216 (ohne) / 1356 (mit) Sekunden voraus. Die Funktion kann also nicht ganz akkurat sein für alle ``c``. Es ist jedoch zu beachten, dass das 7. Beispiel ein Glücksfall ist, da die erste Konfiguration in der Sortierreihenfolge gleich eine stabile ist, sodass die Suche schnell beendet wird. Andere Beispiele mit dem gleichen Seeumfang kommen schon eher and die vorausgesagte Laufzeit heran. Leider gibt es keine Messdaten für größere ``c``, da es schlicht zu lange dauert genügend Versuche durchzuführen.

Schließlich noch ein Blick auf das Laufzeitverhalten im Verhältnis zu k:
"""

# ╔═╡ 5f3d3fe6-e266-4cc1-b6a8-b811e4d411db
img("./complexityk.svg")

# ╔═╡ d32a89d3-0c4d-45b2-87b0-a121724b82e5
md"""
Wie zu erwarten sieht man einen exponentiellen Anstieg gefolgt von einem exponentiellen Abfall. Dies liegt daran, das ``c \choose k`` mit ``O(c^{min(k, c-k)})`` wächst.
"""

# ╔═╡ 449fe15d-6bda-4994-abf1-89d3bacdf6c5
md"""
## Beispiele

Das Programm gibt entweder k Zahlen, die den Positionen der Eisbuden der zuerst gefundenen stabilen Konfiguration entsprechen aus, oder einen Hinweis, dass keine stabile Konfiguration gefunden wurde.

Im folgenden die Ausgaben zu allen vorgegenen Beispielen (für den Fall k=3 wie in der Aufgabenstellung):
```
eisbuden1.txt:
2 8 14

eisbuden2.txt:
Keine stabile Konfiguration gefunden

eisbuden3.txt:
0 17 42

eisbuden4.txt:
Keine stabile Konfiguration gefunden

eisbuden5.txt:
83 128 231

eisbuden6.txt:
Keine stabile Konfiguration gefunden

eisbuden7.txt:
114 285 420
```

Für die Beispiele 2 und 4 lassen sich stabile Konfigurationen mit k=4 finden:

```
eisbuden2.txt:
6 36 39 46

eisbuden4.txt:
25 40 71 92
```

Für Beispiel 6 ließ sich keine Lösung in annehmbarer Zeit finden.

#### Weitere Beispiele

Im Folgenden ein par Beispiele, die Edgecases testen (k=3):

```
Beispiel "Ein Haus pro Schritt":
c=20, n=20, hs=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19]

Ausgabe:
0 6 13

Beispiel "Nur k Häuser"
c=20, n=3, hs=[2, 5, 17]

Ausgabe:
2 5 17

Beispiel "Keine Häuser"
c=20, n=0, hs=[]

Ausgabe:
0 1 2

Beispiel "k < c"
c=2, n=1, hs=[0]

Ausgabe:
Keine stabile Konfiguration gefunden

# Da es keine Konfiguration mit drei Buden auf einem Kreis des Umfangs 2 geben
kann, gibt es auch keine stabile Konfiguration.
```
"""

# ╔═╡ 5fd46a15-6dc1-4ffe-af9b-5f877dd560b7
md"""
## Implementierung


Die Lösung wurde in der Programmiersprache Julia (v1.6) implementiert. Die Entwicklungsumgebung war ein interaktives Pluto.jl Notebook. Es liegt ein Kommandozeilenprogramm vor, dass Beispiele aus STDIN einliest und eine Lösung ausgibt. Es kann mittels `julia main.jl [k] < ./beispiel.txt` (im Aufgabenordner) ausgeführt werden. Da Julia aber lange Kaltstartzeiten hat, lohnt es sich eher, eine interaktive Session wie die Julia REPL oder eben ein Notebook zu nutzen.

Die Datei `eisbudendilemma.jl` im Aufgabenordner entspricht diesem Pluto.jl Notebook.
Um es ausführen zu können muss Julia, Pluto.jl und das Julia-Paket `Combinatorics` installiert werden. Letzteres dient der Generierung von Kombinationen.

Die Datei kann dann, nachdem Pluto.jl gestartet wurde, im Webinterface geöffnet werden. Zur Verwendung des Webinterface siehe Pluto.jl Dokumentation.

Nun zur Implementierung. Im folgenden ist der gesamte Lösungsrelevante Quelltext mit Beschreibungen und Kommentaren aufgeführt.
"""

# ╔═╡ d5a0af62-8714-470a-b9c4-56ff2955de2e
md"""
Hier werden zunächst einige Datentypen definiert, um den nachfolgenden Code lesbarer zu machen.
"""

# ╔═╡ a177ad51-7eb9-4af0-9034-fb29fbb67317
# Type aliases for readability
begin
	# Dtype for all Integer variables (small to minimize memory consumption)
	const IntType = Int16
	const Position = IntType
	# Dtype for one configuration
	const Config = Vector{Position}
	# Dtype for Distances
	const Distances = Vector{Position}
	# Dtype for list of Houses
	const Houses = Vector{Position}
	# Dtype for the memorized sorted configurations
	const ConfMemo = Vector{Config}
end;

# ╔═╡ e02cf83e-7c8c-435f-9e6b-90176aee4d89
md"""
Die Struktur `Lake` beschreibt ein Beispiel. Sie enthält die Parameter `c`, `n`, die Positionen der Häuser `hs` und die gespeicherten Einzelabstände (siehe 1. Optimierung, Absatz 1). Die Funktion `_cost1` berechnet diese. Die Funktion `get_configs` generiert alle möglichen Eisbudenkonfigurationen für ein Beispiel und ein gegebenes k.
"""

# ╔═╡ 97103b7f-8ed3-4b87-984b-a8d0887da497
begin
	struct Lake
		c::Position # Circumference
		n::Position # Number of houses
		hs::Houses  # Positions of houses

		_votes_needed::IntType
		_cost_mem1::Vector{Distances}
	end
	
	positions(c, hs::Houses) = zero(Position):Position(c-1)	
	positions(lake::Lake) = positions(lake.c, lake.hs)
	
	# Calculate the distance of each house to one store on the circle
	function _cost1(hs::Houses, c, hc, store_pos)::Distances
		@. abs(mod(hs - store_pos + hc, c) - hc)
	end
	
	# Create a Lake from the constants c (circumference), n (number of houses)
	# and hs (the positions of the houses
	# also populate the memory of the distance of each house to
	# each point on the circle 
	function Lake(c, n, hs::Any)
		hs_ = convert(Houses, hs)
		m = [_cost1(hs_, c, c / 2.0, p) for p = 0:c-1]
		Lake(c, n, hs_, fld(length(hs), 2) + 1, m)
	end
	
	# Get an iterator of all possible store configurations for k stores
	get_configs(lake::Lake, k) = combinations(positions(lake), k)
end;

# ╔═╡ a8d7d873-f0e0-4070-b9b7-07d4116f8821
md"""So können wir alle vorgegebenen Beispiele definieren."""

# ╔═╡ 87886dde-8676-11eb-283c-953205988317
const examples = [
	Lake(20, 7, [0, 2, 3, 8, 12, 14, 15]),
	Lake(50, 15, [3, 6, 7, 9, 24, 27, 36, 37, 38, 39, 40, 45, 46, 48, 49]),
	Lake(50, 16, [2, 7, 9, 12, 13, 15, 17, 23, 24, 35, 38, 42, 44, 45, 48, 49]),
	Lake(100, 19, [6, 12, 23, 25, 26, 28, 31, 34, 36, 40, 41, 52, 66, 67, 71, 75, 80, 91, 92]),
	Lake(247, 24, [2, 5, 37, 43, 72, 74, 83, 87, 93, 97, 101, 110, 121, 124, 126, 136, 150, 161, 185, 200, 201, 230, 234, 241]),
	Lake(437, 36, [4, 12, 17, 23, 58, 61, 67, 76, 93, 103, 145, 154, 166, 170, 192, 194, 209, 213, 221, 225, 239, 250, 281, 299, 312, 323, 337, 353, 383, 385, 388, 395, 405, 407, 412, 429]),
	Lake(625, 40, [12, 15, 27, 30, 32, 110, 114, 117, 121, 123, 132, 150, 184, 210, 240, 241, 262, 268, 271, 285, 289, 292, 297, 300, 302, 310, 330, 364, 384, 402, 408, 409, 416, 420, 425, 430, 431, 437, 528, 550]),
];

# ╔═╡ 2f4a4dd7-a1c4-49e7-92b2-1e837100c265
md"""
Die Funktion `costn` berechnet die Abstände aller Häuser des Sees zu ihrer nächsten Eisbude.
"""

# ╔═╡ b43ce86b-3780-4a51-80e5-5bb9637174b2
begin
	# Get the distance of each house to the specified position from memory
	cost1(lake::Lake, position::Position) = lake._cost_mem1[position+1]
	
	# Calculate the distance of each house to its closest store in this configuration
	function costn(lake::Lake, config::Config)
		costs = copy(cost1(lake, config[1]))
		
		for i = 2:length(config)
			costi = cost1(lake, config[i])
			for j = 1:lake.n
				costs[j] = min(costs[j], costi[j])
			end
		end
		costs
	end
end;

# ╔═╡ 249d22a5-bc14-4646-997f-dc714620cfbf
md"""
Die Funktion `make_conf_memo` sortiert die von `get_configs` generierten Konfigurationen nach ihrem Gesamtabstand (siehe 2. Optimierung).
"""

# ╔═╡ 2a037da0-5111-4c2d-900d-d2d2afee06b9
# Create all possible configurations and sort them by their combined distance
function make_conf_memo(lake::Lake, k)
	confs::Vector{Config} = collect(get_configs(lake, k))
	sorted = sortperm([sum(costn(lake, conf)) for conf = confs])
	confs[sorted]
end;

# ╔═╡ bf393915-d664-4762-abbb-135a7dc52d3b
md"""
Die Funktionen im nächsten Block dienen der Simulation von Abstimmungen (siehe Simulation der Abstimmung).
"""

# ╔═╡ fe4bb6e0-52b5-4ed8-90f0-0057f857ec5b
begin
	@enum VoteResult A_WON B_WON DRAW
	
	# Simulate a vote between two configurations, but their costn
	# has already been calculated
	function _vote_impl(votes_needed, a::Distances, b::Distances)
		count_lt0 = 0
		count_gt0 = 0
		for i = 1:length(a)
			diff = a[i] - b[i]
			if diff < 0 && (count_lt0 += 1) >= votes_needed
				return A_WON
			elseif diff > 0 && (count_gt0 += 1) >= votes_needed
				return B_WON
			end
		end
		
		return DRAW
	end
	
	# Simulate a vote between the two configurations a and b
	vote(lake::Lake, a::Config, b::Config) =
		_vote_impl(lake._votes_needed, costn(lake, a), costn(lake, b))
	
	# Simulate a vote between the two configurations a and b, but costn for
	# configuration a is precalculated
	vote_pre(lake::Lake, a::Distances, b::Config) =
		_vote_impl(lake._votes_needed, a, costn(lake, b))
end;

# ╔═╡ d17d1778-6991-41c2-8b90-4c21c810dbf3
md"""
Die Funktion `is_stable` prüft unter Verwendung der Optimierungen (außer Minimalabstandsspeicherung, siehe unten), ob eine Konfiguration stabil ist, indem diese gegen alle anderen antritt. Die Funktion bricht ab, sobald eine der Abstimmungen gegen die zu prüfende Konfiguration ausging.
"""

# ╔═╡ e92febec-08d7-46b0-a259-af913f136b4b
# Check if the given configuration is stable
function is_stable(lake::Lake, config::Config, configs::ConfMemo)
	cur_cost = costn(lake, config)

	for new_config = configs
		if vote_pre(lake, cur_cost, new_config) == B_WON
			return false
		end
	end

	true
end;

# ╔═╡ e2555c9a-b332-46f1-bd06-69aed4800043
md"""
Die Funktionen `find_all` und `find_one` dienen der Findung von stabilen Konfigurationen. `find_one` bricht nach der ersten gefundenen stabilen Konfiguration ab, wie oben beschrieben. Falls doch alle stabilen Konfigurationen berechnet werden sollen, kann `find_all` verwendet werden.
"""

# ╔═╡ 0fba10a6-1875-4465-bdd1-b6e4fcd42c90
begin
	# Find all stable configurations of k stores
	function find_all(lake::Lake, k)
		configs = make_conf_memo(lake, k)
		
		[conf for conf = configs if is_stable(lake, conf, configs)]
	end
	
	# Find one stable configuration of k stores
	function find_one(lake::Lake, k)
		configs = make_conf_memo(lake, k)
		
		for conf = configs
			if is_stable(lake, conf, configs)
				return conf
			end
		end
	end
end;

# ╔═╡ 5a3532ec-18f0-4513-98a1-fa5eb4d7ea01
md"""
##### Nun die gleichen Funktionen, unter Verwendung von Minimalabstandspeicherung

Die folgenden Funktionen sind fast identisch mit den oberen, nur dass sie die gespeicherten Minimalabstände beachten (oder diese erzeugen).
"""

# ╔═╡ c2964f72-425f-4f9e-a272-7b5b218e1cb2
begin
	# Get the linear index for the configuration into the distance memory
	function _get_mem_idx(config::Config, c)
		idx = Int64(1)
		_c = Int64(c)
		for (i, v) = enumerate(config)
			idx += (v - 1) * (_c ^ (i - 1))
		end
		idx+1
	end
	
	function costn_mem(lake::Lake, cost_memo, config::Config)
		cost_memo[_get_mem_idx(config, lake.c)][]
	end
	
	vote_pre_mem(lake::Lake, cost_memo, a::Vector{Position}, b::Config) =
		_vote_impl(lake._votes_needed, a, costn_mem(lake, cost_memo, b))
end;

# ╔═╡ d3717c4b-306d-437f-a7fb-5be5686571be
function make_conf_and_cost_memo(lake, k)
	cost_memo = Vector{Ref{Distances}}(undef, Int64(lake.c) ^ Int64(k))

	confs::Vector{Config} = collect(get_configs(lake, k))
	scores = Vector{IntType}(undef, length(confs))
	
	for (i, conf) = enumerate(confs)
		cost = costn(lake, conf)
		cost_memo[_get_mem_idx(conf, lake.c)] = cost
		scores[i] = sum(cost)
	end
	
	sorted = sortperm(scores)
	confs[sorted], cost_memo
end;

# ╔═╡ 8bc6493c-da4c-4973-a17b-e364a90f8bbb
function is_stable_mem(lake::Lake, cost_memo, config::Config, configs::ConfMemo)
	cur_cost = costn(lake, config)

	for new_config = configs
		if vote_pre_mem(lake, cost_memo, cur_cost, new_config) == B_WON
			return false
		end
	end

	true
end;

# ╔═╡ 5f4ea315-53b3-4be4-b6a0-f60c81cd109e
begin
	# Find all stable configurations of k stores using distance memory
	function find_all_mem(lake::Lake, k)
		configs, cost_memo = make_conf_and_cost_memo(lake, k)
		
		[conf for conf = configs if is_stable_mem(lake, cost_memo, conf, configs)]
	end
	
	# Find one stable configuration of k stores using distance memory
	function find_one_mem(lake::Lake, k)
		configs, cost_memo = make_conf_and_cost_memo(lake, k)
		
		for conf = configs
			if is_stable_mem(lake, cost_memo, conf, configs)
				return conf
			end
		end
	end
end;

# ╔═╡ ae7c690d-5f60-4fba-9c88-28f3dd39d1b3
md"""
Die Funktion `solve` implementiert die Entscheidung, ob Minimalabstandsspeicherung eingesetzt werden kann oder nicht. Falls nicht, sollte der Aufruf von `find_one_mem` scheitern, sodass danach regulär `find_one` aufgerufen wird.

Außerdem gibt diese Funktion lesbare Rückgabewerte aus.
"""

# ╔═╡ 5051cd4a-9203-4426-a556-5e3b5a71b096
function solve(example, k)
	result = Nothing
	try # First, try solving with distance memory
		result = find_one_mem(example, k)
	catch # If an out of memory error occured, try without distance memory
		result = find_one(example, k)
	end
	
	if isnothing(result)
		"Keine stabile Konfiguration gefunden"
	else
		join(result, " ")
	end
end;

# ╔═╡ cd3602aa-0d5e-4b2b-9a6c-5a7bcabac7be
md"""
Beispielverwendung:
"""

# ╔═╡ 0d0197fc-3442-42ff-a1b3-fa97f51ad9ce
begin
	c, n, hs = 20, 7, [0,2,3,8,12,14,15]
	solve(Lake(c, n, hs), 3)
end

# ╔═╡ Cell order:
# ╟─528c7bac-8676-11eb-1e17-fff90e96c3c6
# ╟─40371f7f-7415-46a6-b8ce-2e8753341de8
# ╟─e53464a8-8a47-4019-af31-c9a6569f108d
# ╟─9cdf9527-6daf-44fe-b13d-4978f9be1301
# ╟─b6736afd-5524-48f5-a406-498adaf35278
# ╟─abba2b0e-a611-44fe-8175-dc6e1af2c05c
# ╟─4460e7a9-0f46-401d-8171-c03f4762afa6
# ╟─91b04923-d4c9-4a85-a2eb-cddcf8b783a0
# ╟─7ef9de81-b66f-417d-b7cc-8ceebb346520
# ╟─b4606c54-ee79-4bd0-9ca8-c4107cdb67d4
# ╟─a856419b-149a-4eea-ad00-a4cbfd561afe
# ╟─40b4099c-690f-403f-a638-0d0d388d5da9
# ╟─bdeae36c-0daf-44e0-96f8-7226f13dde2b
# ╟─7d4d2336-b5fe-4458-8cff-164ad71a28c8
# ╟─cbdb4b0b-7ef1-4b8e-bac2-5d263887810e
# ╟─5f3d3fe6-e266-4cc1-b6a8-b811e4d411db
# ╟─d32a89d3-0c4d-45b2-87b0-a121724b82e5
# ╟─449fe15d-6bda-4994-abf1-89d3bacdf6c5
# ╟─5fd46a15-6dc1-4ffe-af9b-5f877dd560b7
# ╟─75e64786-8676-11eb-3597-f79f94b517ed
# ╟─d5a0af62-8714-470a-b9c4-56ff2955de2e
# ╠═a177ad51-7eb9-4af0-9034-fb29fbb67317
# ╟─e02cf83e-7c8c-435f-9e6b-90176aee4d89
# ╠═97103b7f-8ed3-4b87-984b-a8d0887da497
# ╟─a8d7d873-f0e0-4070-b9b7-07d4116f8821
# ╠═87886dde-8676-11eb-283c-953205988317
# ╟─2f4a4dd7-a1c4-49e7-92b2-1e837100c265
# ╠═b43ce86b-3780-4a51-80e5-5bb9637174b2
# ╟─249d22a5-bc14-4646-997f-dc714620cfbf
# ╠═2a037da0-5111-4c2d-900d-d2d2afee06b9
# ╟─bf393915-d664-4762-abbb-135a7dc52d3b
# ╠═fe4bb6e0-52b5-4ed8-90f0-0057f857ec5b
# ╟─d17d1778-6991-41c2-8b90-4c21c810dbf3
# ╠═e92febec-08d7-46b0-a259-af913f136b4b
# ╟─e2555c9a-b332-46f1-bd06-69aed4800043
# ╠═0fba10a6-1875-4465-bdd1-b6e4fcd42c90
# ╟─5a3532ec-18f0-4513-98a1-fa5eb4d7ea01
# ╠═c2964f72-425f-4f9e-a272-7b5b218e1cb2
# ╠═d3717c4b-306d-437f-a7fb-5be5686571be
# ╠═8bc6493c-da4c-4973-a17b-e364a90f8bbb
# ╠═5f4ea315-53b3-4be4-b6a0-f60c81cd109e
# ╟─ae7c690d-5f60-4fba-9c88-28f3dd39d1b3
# ╠═5051cd4a-9203-4426-a556-5e3b5a71b096
# ╟─cd3602aa-0d5e-4b2b-9a6c-5a7bcabac7be
# ╠═0d0197fc-3442-42ff-a1b3-fa97f51ad9ce
