#! /usr/bin/env julia

using Combinatorics: combinations;

# Dtype for all Integer variables (small to minimize memory consumption)
const IntType = Int16
const Position = IntType
# Dtype for one configuration
const Config = Vector{Position}
# Dtype for Distances
const Distances = Vector{Position}
# Dtype for list of Houses
const Houses = Vector{Position}
# Dtype for the memorized sorted configurations
const ConfMemo = Vector{Config}

struct Lake
    c::Position # Circumference
    n::Position # Number of houses
    hs::Houses  # Positions of houses

    _votes_needed::IntType
    _cost_mem1::Vector{Distances}
end

positions(c, hs::Houses) = zero(Position):Position(c-1)	
positions(lake::Lake) = positions(lake.c, lake.hs)

# Calculate the distance of each house to one store on the circle
function _cost1(hs::Houses, c, hc, store_pos)::Distances
    @. abs(mod(hs - store_pos + hc, c) - hc)
end

# Create a Lake from the constants c (circumference), n (number of houses)
# and hs (the positions of the houses
# also populate the memory of the distance of each house to
# each point on the circle 
function Lake(c, n, hs::Any)
    hs_ = convert(Houses, hs)
    m = [_cost1(hs_, c, c / 2.0, p) for p = 0:c-1]
    Lake(c, n, hs_, fld(length(hs), 2) + 1, m)
end

# Get an iterator of all possible store configurations for k stores
get_configs(lake::Lake, k) = combinations(positions(lake), k)

const examples = [
	Lake(20, 7, [0, 2, 3, 8, 12, 14, 15]),
	Lake(50, 15, [3, 6, 7, 9, 24, 27, 36, 37, 38, 39, 40, 45, 46, 48, 49]),
	Lake(50, 16, [2, 7, 9, 12, 13, 15, 17, 23, 24, 35, 38, 42, 44, 45, 48, 49]),
	Lake(100, 19, [6, 12, 23, 25, 26, 28, 31, 34, 36, 40, 41, 52, 66, 67, 71, 75, 80, 91, 92]),
	Lake(247, 24, [2, 5, 37, 43, 72, 74, 83, 87, 93, 97, 101, 110, 121, 124, 126, 136, 150, 161, 185, 200, 201, 230, 234, 241]),
	Lake(437, 36, [4, 12, 17, 23, 58, 61, 67, 76, 93, 103, 145, 154, 166, 170, 192, 194, 209, 213, 221, 225, 239, 250, 281, 299, 312, 323, 337, 353, 383, 385, 388, 395, 405, 407, 412, 429]),
	Lake(625, 40, [12, 15, 27, 30, 32, 110, 114, 117, 121, 123, 132, 150, 184, 210, 240, 241, 262, 268, 271, 285, 289, 292, 297, 300, 302, 310, 330, 364, 384, 402, 408, 409, 416, 420, 425, 430, 431, 437, 528, 550]),
];


# Get the distance of each house to the specified position from memory
cost1(lake::Lake, position::Position) = lake._cost_mem1[position+1]

# Calculate the distance of each house to its closest store in this configuration
function costn(lake::Lake, config::Config)
    costs = copy(cost1(lake, config[1]))
    
    for i = 2:length(config)
        costi = cost1(lake, config[i])
        for j = 1:lake.n
            costs[j] = min(costs[j], costi[j])
        end
    end
    costs
end

# Create all possible configurations and sort them by their combined distance
function make_conf_memo(lake::Lake, k)
	confs::Vector{Config} = collect(get_configs(lake, k))
	sorted = sortperm([sum(costn(lake, conf)) for conf = confs])
	confs[sorted]
end;

@enum VoteResult A_WON B_WON DRAW

# Simulate a vote between two configurations, but their costn
# has already been calculated
function _vote_impl(votes_needed, a::Distances, b::Distances)
    count_lt0 = 0
    count_gt0 = 0
    for i = 1:length(a)
        diff = a[i] - b[i]
        if diff < 0 && (count_lt0 += 1) >= votes_needed
            return A_WON
        elseif diff > 0 && (count_gt0 += 1) >= votes_needed
            return B_WON
        end
    end
    
    return DRAW
end

# Simulate a vote between the two configurations a and b
vote(lake::Lake, a::Config, b::Config) =
    _vote_impl(lake._votes_needed, costn(lake, a), costn(lake, b))

# Simulate a vote between the two configurations a and b, but costn for
# configuration a is precalculated
vote_pre(lake::Lake, a::Distances, b::Config) =
    _vote_impl(lake._votes_needed, a, costn(lake, b))

# Check if the given configuration is stable
function is_stable(lake::Lake, config::Config, configs::ConfMemo)
	cur_cost = costn(lake, config)

	for new_config = configs
		if vote_pre(lake, cur_cost, new_config) == B_WON
			return false
		end
	end

	true
end;

# Find all stable configurations of k stores
function find_all(lake::Lake, k)
    configs = make_conf_memo(lake, k)
    
    [conf for conf = configs if is_stable(lake, conf, configs)]
end

# Find one stable configuration of k stores
function find_one(lake::Lake, k)
    configs = make_conf_memo(lake, k)
    
    for conf = configs
        if is_stable(lake, conf, configs)
            return conf
        end
    end
end

# Get the linear index for the configuration into the distance memory
function _get_mem_idx(config::Config, c)
    idx = Int64(1)
    _c = Int64(c)
    for (i, v) = enumerate(config)
        idx += (v - 1) * (_c ^ (i - 1))
    end
    idx+1
end

function costn_mem(lake::Lake, cost_memo, config::Config)
    cost_memo[_get_mem_idx(config, lake.c)][]
end

vote_pre_mem(lake::Lake, cost_memo, a::Vector{Position}, b::Config) =
    _vote_impl(lake._votes_needed, a, costn_mem(lake, cost_memo, b))

function make_conf_and_cost_memo(lake, k)
	cost_memo = Vector{Ref{Distances}}(undef, Int64(lake.c) ^ Int64(k))

	confs::Vector{Config} = collect(get_configs(lake, k))
	scores = Vector{IntType}(undef, length(confs))
	
	for (i, conf) = enumerate(confs)
		cost = costn(lake, conf)
		cost_memo[_get_mem_idx(conf, lake.c)] = cost
		scores[i] = sum(cost)
	end
	
	sorted = sortperm(scores)
	confs[sorted], cost_memo
end;

function is_stable_mem(lake::Lake, cost_memo, config::Config, configs::ConfMemo)
	cur_cost = costn(lake, config)

	for new_config = configs
		if vote_pre_mem(lake, cost_memo, cur_cost, new_config) == B_WON
			return false
		end
	end

	true
end;

# Find all stable configurations of k stores using distance memory
function find_all_mem(lake::Lake, k)
    configs, cost_memo = make_conf_and_cost_memo(lake, k)
    
    [conf for conf = configs if is_stable_mem(lake, cost_memo, conf, configs)]
end

# Find one stable configuration of k stores using distance memory
function find_one_mem(lake::Lake, k)
    configs, cost_memo = make_conf_and_cost_memo(lake, k)
    
    for conf = configs
        if is_stable_mem(lake, cost_memo, conf, configs)
            return conf
        end
    end
end

function solve(example, k)
	result = Nothing
	try # First, try solving with distance memory
		result = find_one_mem(example, k)
	catch # If an out of memory error occured, try without distance memory
		result = find_one(example, k)
	end
	
	if isnothing(result)
		"Keine stabile Konfiguration gefunden"
	else
		join(result, " ")
	end
end;

function main()
    params, houses = split.(chomp.(readlines())[1:2])
    c, n = parse.(Int, params)
    hs = parse.(Int, houses)
    lake = Lake(c, n, hs)

    k = 3
    if length(ARGS) > 0
        k = parse(Int, ARGS[1])
    end

    println(solve(lake, k))
end

main()
